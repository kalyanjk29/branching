#!/bin/bash

##########################################################################################
#Purpose of the Script


#releasebranch.sh  <Repository_URL> <RELEASEBRANCHNAME> <VERSION> <MAILLIST>
#example: releasebranch.sh  git@github.intuit.com:Devtest/webs-pri-subscription-event-publisher-v1.git  RT3.1-20150213 1.2.3.4-SNAPSHOT ajith_kattil@intuit.com
#if [ -d "$WORKSPACE" ]; then
#  rm -rf $WORKSPACE
#fi

echo "$WORKSPACE removed"
Repository_URL=$1
Branch=$2
Branch_name=$3
Branch_version=$4
Email_DL=$5

NOW=$(date +"%Y.%m.%d")
TAGNAME="$Branch_name-$NOW" 
PROJECT=$(basename $Repository_URL .git)
#echo "PROJECT=${PROJECT}"

echo "#####################################################################################"
echo "....................................................................."
echo "... Validate if REPO $PROJECT Exists on GITHUB ..."
echo "....................................................................."
echo "#####################################################################################"

#git ls-remote ${Repository_URL}

# Remote exists - Exit script if remote does not exist 

VAL=`git ls-remote ${Repository_URL} HEAD`
if [[ $VAL == *"HEAD"* ]]; then
    echo " Repository contains 'HEAD'";
else
    	echo "....................................................................."
	echo "... Repository $PROJECT does not exist OR you might have cloned via https ..."
	echo "... Please Try again with a valid Repository URL ..."
	echo "... Exit Script ..."
	echo "....................................................................."
    	exit 1
fi

echo "......................................................................."
echo "... REPO $PROJECT Exists on GITHUB. Clonning ..."
echo "......................................................................."

## Cloning the Repository ##
#git clone ${Repository_URL} .
git clone ${Repository_URL}

rm -r *.py *.sh README.md inputfile

#if [ -d "$PROJECT" ]; then
#	echo "FATAL !!! THIS RELEASE BRANCH ALREADY EXISTS"
#	echo  "Exiting the script"
#	exit 1
#fi
#REPOTEMP=${Repository_URL%/$PROJECT}
#ORG=Devtest
#ORG="${REPOTEMP##*/}" ## uncomment this
#echo "ORG= ${ORG}"

#ORG="${TEMP##*/}"
#echo ${Repository_URL%.PROJECT}
#echo " ORG=$ORG"
echo "....................................................................."
echo "...... Listing all the current branches for the Repository ........"
echo "....................................................................."

#git remote -v
git ls-remote ${Repository_URL}

cd $PROJECT
# Branching done here
#git push origin origin:refs/heads/${Release_branch}
#git init
#git fetch origin

## Showing the remotes branches for the current repository ##
echo "....................................................................."
echo "... Showing all Remote branches for $PROJECT ..."
echo "...................................................................."

git branch -r
#git checkout --track -b ${Release_branch} origin/${Release_branch}
#git checkout --track -b ${Release_branch}


##########################################################################################
# VERIFY WHETHER THE BRANCH exists
echo "#####################################################################################"
echo "....................................................................."
echo "... Validate if $Branch_name Exists for $PROJECT ....."
echo "....................................................................."

#hasBranch=`git show-ref refs/heads/${Release_branch}`;
branches=`git branch -r | grep ${Branch_name}`
branch=${branches##*/}

if [[ $branch == $Branch_name ]]; then
  echo "Branch $Branch_name Exists"
  git checkout $Branch_name
  echo "Validate version"
pwd
version_in_pom=`grep -A10 "</parent>" ./pom.xml | grep -m1 version | sed 's|.*<version>\(.*\)</version>|\1|g'`
 echo ""$version_in_pom
 echo ""$Branch_version
if [[ $Branch_version == $version_in_pom ]]; then
  echo ".. Version matched. Cannot create a version that already Exists .. "
  exit 1
else
  echo "Version not in pom"
fi
else 
  echo "Branch Does not exist .. Creating now "
  git checkout -b $Branch_name
fi


#release=$Release_branch
git checkout $Branch_name

#git checkout -b ${Release_branch}

## Tagging the Release Branch ##
#git tag $TAGNAME
#git push origin tag $TAGNAME

## Deleting the Branch and Tag for the REPO ##
#git push origin :[branch_name]
## To delete locally ## forceful ##
# git push oring :(tagname)
#git branch -D [branch_name]

echo "execute permissions to the files.."
pwd

#chmod +x pom.xml
#mvn -gs /app/maven/apache-maven-3.2.2/conf/settings_test.xml clean

#/app/maven/apache-maven-3.2.2/bin/mvn -v
/app/maven/apache-maven-3.2.2/bin/mvn versions:set -DnewVersion=$Branch_version

## Invoke Release Prepare ##
#/app/maven/apache-maven-3.2.2/bin/mvn -B release:prepare
#mvn release:clean release:prepare -Dresume=false -DdryRun=true -X
#mvn release:branch -DbranchName=my-branch -DupdateBranchVersions=true -DupdateWorkingCopyVersions=false -DautoVersionSubmodules=true
git add -u
git commit -am "resolve snapshot"

## Email notifications ##
echo -ne 'notifications:\n  email:\n    recipients:\n      - '${Email_DL}'' >> build.yml

git add build.yml
git commit -m "adding email notifications"
#git push origin origin:refs/heads/${Release_branch}

git push -u origin $Branch_name

#git checkout master
#mvn -B release:prepare
#git add pom.xml
#git commit -m "Appending build no"

## EnD of Script ##
